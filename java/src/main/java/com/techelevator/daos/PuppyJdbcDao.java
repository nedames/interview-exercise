package com.techelevator.daos;

import com.techelevator.models.Puppy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

/**
 * PuppyJdbcDao
 */
@Component
public class PuppyJdbcDao implements PuppyDao {

    private NamedParameterJdbcTemplate template;

    public PuppyJdbcDao(DataSource datasource) {
        template = new NamedParameterJdbcTemplate(datasource);
    }

    @Override
    public List<Puppy> getPuppies() {
        return template.query("SELECT * FROM puppies", new BeanPropertyRowMapper<>(Puppy.class));
    }

    @Override
    public Puppy getPuppy(int id) {
        SqlParameterSource parameters = new MapSqlParameterSource("id", Integer.valueOf(id));
        return template.queryForObject("SELECT * FROM puppies WHERE id=:id", parameters, new BeanPropertyRowMapper<>(Puppy.class));
    }

    @Override
    public void savePuppy(Puppy puppyToSave) {
        final String INSERT_SQL =
                "INSERT INTO puppies(name, weight, gender, paper_trained) VALUES(:name, :weight, :gender, :paper_trained)";

        SqlParameterSource parameters =
                new MapSqlParameterSource("name", String.valueOf(puppyToSave.getName()))
                        .addValue("weight", Integer.valueOf(puppyToSave.getWeight()))
                        .addValue("gender", String.valueOf(puppyToSave.getGender()))
                        .addValue("paper_trained", Boolean.valueOf(puppyToSave.isPaperTrained()));

        template.update(INSERT_SQL, parameters);

        return;
    }
}