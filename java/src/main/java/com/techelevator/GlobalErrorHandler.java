package com.techelevator;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import org.apache.log4j.Logger;

@ControllerAdvice
@RequestMapping()
public class GlobalErrorHandler {

    static Logger log = Logger.getLogger(GlobalErrorHandler.class);

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<String> handleException(Exception ex) {
        // Log the details for your team
        log.error(ex.getMessage());
        // Show the user what they need to see
        return new ResponseEntity<String>("Something Unfortunate Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
