package com.techelevator.controllers;

import com.techelevator.daos.PuppyDao;
import com.techelevator.models.Puppy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import java.util.List;

/**
 * PuppiesController
 */
@Controller
public class PuppiesController {

    PuppyDao puppyDao;

    @Autowired
    public PuppiesController(PuppyDao puppyDao) {
        this.puppyDao = puppyDao;
    }

    @GetMapping("/")
    public ModelAndView getPuppies() {
        List<Puppy> puppies = puppyDao.getPuppies();
        ModelAndView mav = new ModelAndView();
        mav.addObject("puppies", puppies);
        mav.setViewName("puppyList");
        return mav;
    }

    @GetMapping("/puppy")
    public ModelAndView getPuppy(@RequestParam int id) {
        Puppy puppy = puppyDao.getPuppy(id);
        ModelAndView mav = new ModelAndView();
        mav.addObject("puppy", puppy);
        mav.setViewName("puppyDetail");
        return mav;
    }

    @PostMapping("/puppies")
    public String savePuppy(@ModelAttribute("puppy") Puppy newPuppy, BindingResult bindingResult) throws Exception {
        if (!bindingResult.hasErrors()) {
            puppyDao.savePuppy(newPuppy);
        } else {
            throw new Exception("Error Binding Puppy Object");
        }

        return "redirect:/";
    }
}
