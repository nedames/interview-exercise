# Instructor Interview Assessment

As a part of the Tech Elevator instructor interview process, we would like for you to complete an assessment that we provide to our students. You should produce a solution that you feel demonstrates what would be considered a best practice and a suitable reference for junior developers.

The assessment is available as an ASP.NET MVC or Spring MVC application. You may choose either technology. Within the `dotnet` and `java` folders is a README with instructions. 

The scope of our MVC module covers the following concepts:

* Controllers and Views
* Razor/JSTL
* HTTP GET & HTTP POST
* Model binding
* Dependency Injection
* Session
* Validation
* Data Access Objects

Although indicated in the instructions, you do not need to time-box yourself to one hour. This is the amount of time allowed for students to complete the assessment.

## Submission Requirements

Please follow these steps to work on the assessment and submit your work.

1. Create a private fork of this repository.
2. Clone the fork to your local machine and begin working on the assessment.
3. Commit and push your completed work into your fork.
4. Create a pull request from your fork to `te-curriculum/interview-exercise`. In the pull request make sure that **David Wintrich**(david@techelevator.com) and **Josh Tucholski**(josh@techelevator.com) are listed as reviewers.

After we receive your pull request, we will review it and follow up with next steps.